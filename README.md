# StudyAgenda
This is a to-do list application with a cat illustration theme. Users can add tasks with a name and due date, and mark 
them as completed. The application also includes a button that generates a cat meow sound and redirects the user to a 
YouTube playlist of motivational videos.<br>
Take a look at our [application DEMO video](https://drive.google.com/file/d/1aKcFHcwJLBnmDCgr2YSJyun1YyqioqfW/view)!


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes.

### Prerequisites
- Android Studio
- Java Development Kit (JDK)

### Installing
1. Clone the repository to your local machine using Git or download the zip file.
2. Open the project in Android Studio.
3. Build and run the application on an emulator or connected device.

### Main File
The main file for the application is `MainActivity.java`, located in the package `com.example.mystudyagenda`. This file 
contains the code for the main activity of the application, including the task list, motivation button, and functionality 
for adding new tasks.

### Built With
- [**Android Studio**](https://developer.android.com/studio) - Development environment, version 2121.3.1 
- [**Java**](https://www.java.com/en/) - Version 19.0.1

### Authors
- **Isadora R. Boff**  - Dévellopeur
- **Gabriela G. Baena** - Dévellopeur

### Acknowledgments
Cat meow sound from [freesound.org](https://freesound.org/)<br>
YouTube playlist of motivational videos - [link](https://www.youtube.com/watch?v=IcrbM1l_BoI&list=PLB8RrVmBSp9PguXrA2ixhm0GXptyToPoL)
