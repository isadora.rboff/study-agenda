package com.example.mystudyagenda;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
//import android.util.Log;
//import android.util.SparseBooleanArray;
import android.view.View;
//import android.widget.ArrayAdapter;
import android.widget.Button;
//import android.widget.CheckBox;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
//import java.util.Iterator;

public class MainActivity extends AppCompatActivity  {
    ArrayList<String> taskList = new ArrayList(Arrays.asList("Study Math", "Buy Paper"));
    ArrayList<String> taskDeadline = new ArrayList(Arrays.asList("12/02/23", "15/03/23"));


    ListView listView;
    CustomBaseAdapter adapter;
    Button motivation;
    MediaPlayer mp;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String tasksList_string = data.getStringExtra("keyList");
        String tasksDeadline_string = data.getStringExtra("keyDeadline");

        taskList.add(tasksList_string);
        taskDeadline.add(tasksDeadline_string);
        adapter.notifyDataSetChanged();

        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.out.println("create");
        mp = MediaPlayer.create(this,R.raw.cat);
        motivation = findViewById(R.id.motivation);
        motivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mp.start();
                goLink("https://www.youtube.com/watch?v=IcrbM1l_BoI&list=PLB8RrVmBSp9PguXrA2ixhm0GXptyToPoL");
            }
        });

        listView = (ListView) findViewById(R.id.tasksList);
        adapter = new CustomBaseAdapter(getApplicationContext(), taskList, taskDeadline);
        listView.setAdapter(adapter);


        Button AddTask = (Button)findViewById(R.id.AddTask);
        AddTask.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, AddTaskActivity.class);
            startActivityForResult(intent, 0);
        });


        //Button DeleteTask = (Button)findViewById(R.id.DeleteTask);
        //DeleteTask.setOnClickListener(view -> {
        //    ArrayList<Integer> arrExc = new ArrayList<Integer>();
        //    for(int i = 0; i < 2;i++){

        //        if((CheckBox)listView.getChildAt(i).findViewById(R.id.cb) != null){

        //            CheckBox cBox=(CheckBox)listView.getChildAt(i).findViewById(R.id.cb);

        //            if(cBox.isChecked()){

        //                arrExc.add(i);
        //            }
        //        }
        //    }

        //    Iterator<String> taskListIterator = taskList.iterator();
        //    Iterator<String> taskDeadlineIterator = taskDeadline.iterator();
        //    int i = 0;

        //    while (taskListIterator.hasNext() && taskDeadlineIterator.hasNext()) {

        //        if (true) {

        //            taskListIterator.remove();
        //            //taskDeadlineIterator.remove();
        //            System.out.println(i);
        //        }
        //        taskListIterator.next();
        //        taskDeadlineIterator.next();
        //        i++;
        //    }
        //    adapter.notifyDataSetChanged();
        //});

    }

    private void goLink(String s) {
        Uri uri = Uri.parse(s);
        startActivity(new Intent(Intent.ACTION_VIEW,uri));
    }
}