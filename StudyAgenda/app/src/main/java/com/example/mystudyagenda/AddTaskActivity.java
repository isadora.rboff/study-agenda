package com.example.mystudyagenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

public class AddTaskActivity extends AppCompatActivity {
    private TextInputLayout tasksList, tasksDeadline;
    public Button addv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        tasksList = findViewById(R.id.Input1);
        tasksDeadline = findViewById(R.id.Input2);
        addv = findViewById(R.id.add);

        addv.setOnClickListener(view -> {
            String taskList_string = tasksList.getEditText().getText().toString();
            String taskDeadline_string = tasksDeadline.getEditText().getText().toString();

            Intent intent = new Intent(AddTaskActivity.this, MainActivity.class);
            intent.putExtra("keyList",taskList_string);
            intent.putExtra("keyDeadline", taskDeadline_string);
            setResult(RESULT_OK, intent);
            finish();
        });

    }


}