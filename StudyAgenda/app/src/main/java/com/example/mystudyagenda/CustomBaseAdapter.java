package com.example.mystudyagenda;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CustomBaseAdapter extends BaseAdapter{

    Context context;
    ArrayList<String> taskList;
    ArrayList<String> taskDeadline;
    LayoutInflater inflater;

    public CustomBaseAdapter(Context ctx, ArrayList<String> listTask, ArrayList<String> listDeadline) {
        this.context = ctx;
        this.taskList = listTask;
        this.taskDeadline = listDeadline;
        inflater = LayoutInflater.from(ctx);
    }


    @Override
    public int getCount() {
        return taskList.size();
    }

    @Override
    public Object getItem(int i) {
        return taskList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.activity_listview, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.Task = convertView.findViewById(R.id.tasksList);
            viewHolder.Dead = convertView.findViewById(R.id.tasksDeadline);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final String item = taskList.get(position);

        viewHolder.Task.setText(item);
        viewHolder.Dead.setText(taskDeadline.get(position));
        convertView.setOnClickListener(v -> Toast.makeText(context, "You Just Clicked " + item, Toast.LENGTH_LONG).show());
        return convertView;
    }

    static class ViewHolder {
        TextView Task;
        TextView Dead;
    }

}
