package com.example.mystudyagenda;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class customlistview extends AppCompatActivity {
    private CheckBox checkbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        MediaPlayer mp;
        mp = MediaPlayer.create(this,R.raw.cat);
        checkbox = findViewById(R.id.cb);
        checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkbox.isChecked())
                    mp.start();
                else;
            }
        });

    }
}